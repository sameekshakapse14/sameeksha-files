from frappe import _

def get_data():
	return [
		{
			"module_name": "Assignment",
			"color": "grey",
			"icon": "#",
			"type": "module",
			"label": _("Assignment")
		}
	]
